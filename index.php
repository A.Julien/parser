<?php

require("MagicParser.php");


function myRecordHandler($record ){
	
	/* initialisation des variables */
	$i = 0;
	$produit = array();
	$colonne ="";
	$createColonne ="";
	$values ="";
	
	/* Connection à la BDD */
	$login = 'root';
	$pass = '';
	$bdd = new PDO( 'mysql:host=localhost;dbname=catalogue;charset=utf8', $login, $pass, array( PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION ) );

 	foreach ($record as $key => $value) {
 		if ( $key != "FLIGNE"){
 			/* Typage des colonnes */
 			if ( $key == 'FICTECH_MEMOCAT' ){
 				$createColonne .= " ".$key." TEXT,";
 			}
 			else {
 				$createColonne .= " ".$key." VARCHAR(255),";	
 			}

	 		/* Concaténation des colonnes et valeurs pour les requêtes SQL */
			if ( isset($value) && $value != ""){
				$value = str_replace( "'", "\'", $value); // Echappement des " ' " pour la requête.
				$colonne .= " ".$key.",";
				$values .= " '".$value."',";
			}
		}
 	}

 	/* Requêtes SQL */
 	if( isset($colonne)  && isset($values)) {
		try {
			/* Création de la table si elle n'existe pas */
			$creation = $bdd->query("CREATE TABLE IF NOT EXISTS produit (".substr($createColonne, 0, -1).")");
			/* Ajout d'une entrée */
			$query = $bdd->query( " INSERT INTO produit (".substr($colonne, 0, -1).") VALUES (".substr($values, 0, -1)."); ");
		} 
		catch( Exception $e ) {
			die( $e->getMessage() );
		}
	}
 }

MagicParser_parse("catalogue.xml","myRecordHandler","xml|HF_DOCUMENT/FLIGNE/"); 	

?>